
from rest_framework import serializers
from  todo.models import Todo


class TodoSerializer(serializers.ModelSerializer):

    class Meta:
        model = Todo
        fields =['id','title','description','levels']



class TodoDetailSerializer(serializers.ModelSerializer):
    user = serializers.ReadOnlyField(source='user.username')
    class Meta:
        model = Todo
        fields =['id','title','description','levels','created_at','modified_date','user']
