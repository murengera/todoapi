from django.db import models
import uuid
from django.conf import settings

class Todo(models.Model):
  id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
  title=models.CharField(max_length=100,null=False, blank=False)
  description=models.TextField()
  priority_choices = (
      ('LOW', 'LOW'),
      ('MEDIUM', 'MEDIUM'),
      ('HIGH', 'HIGH')
  )
  levels = models.CharField(max_length=15, choices=priority_choices, default="LOW")
  created_at = models.DateField(auto_now_add=True, null=False, blank=False)
  modified_date = models.DateTimeField(auto_now=True)
  user=models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, blank=True)



  def __str__(self):
    return  self.title
