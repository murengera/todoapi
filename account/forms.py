from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from account.models import User


class UserCreationForms(UserCreationForm):
    class Meta(UserCreationForm):
        model = User
        fields = ('username',)


class UserChangeForms(UserChangeForm):
    class meta:
        model = User
        fields = ('email', 'username',)
